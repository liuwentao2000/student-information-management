package com.liu.service;

import com.liu.mapper.StudentMapper;
import com.liu.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public List<Student> queryAllStudent() {
        return studentMapper.queryAllStudent();
    }

    @Override
    public Student queryByIdName(String studentId, String studentPwd) {
        return studentMapper.queryByIdName(studentId,studentPwd);
    }

    @Override
    public int updateById(String studentId, String studentName, int studentAge, String studentBuild, String studentDormitory, String wish,String studentPwd) {
        return studentMapper.updateById(studentId,studentName,studentAge,studentBuild,studentDormitory,wish,studentPwd);
    }

    @Override
    public List<Student> queryPageStudent(int page) {
        return studentMapper.queryPageStudent(page);
    }

    @Override
    public Student queryStudentById(String studentId) {
        return studentMapper.queryStudentById(studentId);
    }

    @Override
    public int queryWishPost() {
        return studentMapper.queryWishPost();
    }

    @Override
    public int queryWishEmploy() {
        return studentMapper.queryWishEmploy();
    }

    @Override
    public int queryWishBusiness() {
        return studentMapper.queryWishBusiness();
    }

    @Override
    public int queryWishOther() {
        return studentMapper.queryWishOther();
    }

    @Override
    public List<Student> queryProvince(String studentProvince) {
        return studentMapper.queryProvince(studentProvince);
    }

    @Override
    public List<Student> queryInstructor(String instructor) {
        return studentMapper.queryInstructor(instructor);
    }

    @Override
    public List<Student> queryWish(String wish) {
        return studentMapper.queryWish(wish);
    }

    @Override
    public List<String> queryGrade(String studentMajor) {
        return studentMapper.queryGrade(studentMajor);
    }

    @Override
    public List<String> queryClass(String studentMajor, String studentGrade) {
        return studentMapper.queryClass(studentMajor,studentGrade);
    }

    @Override
    public List<Student> queryStudentByMajor(String studentMajor) {
        return studentMapper.queryStudentByMajor(studentMajor);
    }

    @Override
    public List<Student> queryStudentByGrade(String studentMajor, String studentGrade) {
        return studentMapper.queryStudentByGrade(studentMajor,studentGrade);
    }

    @Override
    public List<Student> queryStudentByClass(String studentMajor, String studentGrade, String studentClass) {
        return studentMapper.queryStudentByClass(studentMajor,studentGrade,studentClass);
    }

    @Override
    public List<Student> queryStudentByWish(String studentMajor, String studentGrade, String studentClass, String wish) {
        return studentMapper.queryStudentByWish(studentMajor,studentGrade,studentClass,wish);
    }

    @Override
    public int queryStudentPost(String studentMajor, String studentGrade, String studentClass) {
        return studentMapper.queryStudentPost(studentMajor,studentGrade,studentClass);
    }

    @Override
    public int queryStudentEmployee(String studentMajor, String studentGrade, String studentClass) {
        return studentMapper.queryStudentEmployee(studentMajor,studentGrade,studentClass);
    }

    @Override
    public int queryStudentBusiness(String studentMajor, String studentGrade, String studentClass) {
        return studentMapper.queryStudentBusiness(studentMajor,studentGrade,studentClass);
    }

    @Override
    public int queryStudentOther(String studentMajor, String studentGrade, String studentClass) {
        return studentMapper.queryStudentOther(studentMajor,studentGrade,studentClass);
    }

    @Override
    public int addStudent(Student student) {
        return studentMapper.addStudent(student);
    }

    @Override
    public int updateStudent(Student student) {
        return studentMapper.updateStudent(student);
    }

    @Override
    public int deleteById(String studentId) {
        return studentMapper.deleteById(studentId);
    }
}
