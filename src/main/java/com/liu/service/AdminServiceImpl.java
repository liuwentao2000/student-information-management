package com.liu.service;

import com.liu.mapper.AdminMapper;
import com.liu.pojo.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin queryAllAdmin() {
        return adminMapper.queryAllAdmin();
    }

    @Override
    public Admin queryByIdName(String adminId, String adminPwd) {
        return adminMapper.queryByIdName(adminId,adminPwd);
    }
}
