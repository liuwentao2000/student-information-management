package com.liu.service;

import com.liu.pojo.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StudentService {
    //查询所有学生信息
    List<Student> queryAllStudent();

    //根据id和密码登录
    Student queryByIdName(String studentId, String studentPwd);

    //根据id修改学生信息
    int updateById(String studentId,String studentName,int studentAge,
                   String studentBuild,String studentDormitory,String wish,String studentPwd);

    //实现分页查询
    List<Student> queryPageStudent(int page);

    //根据Id查询学生
    Student queryStudentById(String studentId);

    //查询考研意愿的人数
    int queryWishPost();
    //查询就业学生的人数
    int queryWishEmploy();
    //查询创业学生的人数
    int queryWishBusiness();
    //查询其他学生人数
    int queryWishOther();

    //根据省份查询学生
    List<Student> queryProvince(String studentProvince);

    //根据辅导员查询学生
    List<Student> queryInstructor(String instructor);

    //根据毕业愿望查询学生
    List<Student> queryWish(String wish);

    //根据专业查询年级
    List<String> queryGrade(String studentMajor);

    //根据专业，年级查询班级
    List<String> queryClass(String studentMajor,String studentGrade);

    //根据专业查询所有学生
    List<Student> queryStudentByMajor(String studentMajor);

    //根据专业年级查询学生
    List<Student> queryStudentByGrade(String studentMajor,String studentGrade);

    //根据专业年级班级查询所有学生
    List<Student> queryStudentByClass(String studentMajor,String studentGrade,String studentClass);

    //根据专业年级班级意愿查询学生
    List<Student> queryStudentByWish(String studentMajor,String studentGrade,String studentClass,String wish);

    //查询特定班级学生的考研人数
    int queryStudentPost(String studentMajor,String studentGrade,String studentClass);
    //查询特定班级学生的就业人数
    int queryStudentEmployee(String studentMajor,String studentGrade,String studentClass);
    //查询特定班级学生的创业人数
    int queryStudentBusiness(String studentMajor,String studentGrade,String studentClass);
    //查询特定班级学生的其他人数
    int queryStudentOther(String studentMajor,String studentGrade,String studentClass);

    //添加一名学生
    int addStudent(Student student);

    //管理员修改学生
    int updateStudent(Student student);

    //根据学生Id删除学生
    int deleteById(String studentId);

}
