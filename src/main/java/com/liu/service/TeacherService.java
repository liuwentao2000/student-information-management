package com.liu.service;

import com.liu.pojo.Teacher;

public interface TeacherService {

    //查询所有教师
    Teacher queryAllTeacher();

    //根据教室Id和密码登录
    Teacher queryByIdName(String teacherId,String teacherPwd);

}
