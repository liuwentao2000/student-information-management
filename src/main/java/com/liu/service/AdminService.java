package com.liu.service;

import com.liu.pojo.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

public interface AdminService {
    Admin queryAllAdmin();

    //根据id和密码查询
    Admin queryByIdName(String adminId, String adminPwd);
}
