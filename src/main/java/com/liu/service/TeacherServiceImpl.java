package com.liu.service;

import com.liu.mapper.TeacherMapper;
import com.liu.pojo.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public Teacher queryAllTeacher() {
        return teacherMapper.queryAllTeacher();
    }

    @Override
    public Teacher queryByIdName(String teacherId, String teacherPwd) {
        return teacherMapper.queryByIdName(teacherId,teacherPwd);
    }
}
