package com.liu.mapper;

import com.liu.pojo.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper

public interface TeacherMapper {

    //查询所有教师
    Teacher queryAllTeacher();

    //根据教室Id和密码登录
    Teacher queryByIdName(@Param("teacherId") String teacherId,@Param("teacherPwd") String teacherPwd);
}
