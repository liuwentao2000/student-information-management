package com.liu.mapper;

import com.liu.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface AdminMapper {
    Admin queryAllAdmin();
    //根据id和密码查询
    Admin queryByIdName(@Param("adminId") String adminId,@Param("adminPwd") String adminPwd);
}
