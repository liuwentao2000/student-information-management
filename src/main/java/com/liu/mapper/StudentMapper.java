package com.liu.mapper;

import com.liu.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface StudentMapper {

    //查询所有学生信息
    List<Student> queryAllStudent();

    //根据id和密码登录
    Student queryByIdName(@Param("studentId") String studentId,@Param("studentPwd") String studentPwd);

    //根据id修改学生信息
    int updateById(@Param("studentId") String studentId,@Param("studentName") String studentName,@Param("studentAge") int studentAge,
                   @Param("studentBuild") String studentBuild,@Param("studentDormitory") String studentDormitory,@Param("wish") String wish,@Param("studentPwd") String studentPwd);

    //实现分页查询
    List<Student> queryPageStudent(@Param("page") int page);

    //根据Id查询学生
    Student queryStudentById(@Param("studentId") String studentId);

    //查询考研意愿的人数
    int queryWishPost();
    //查询就业学生的人数
    int queryWishEmploy();
    //查询创业学生的人数
    int queryWishBusiness();
    //查询其他学生人数
    int queryWishOther();

    //根据省份查询学生
    List<Student> queryProvince(@Param("studentProvince") String studentProvince);

    //根据辅导员查询学生
    List<Student> queryInstructor(@Param("instructor") String instructor);

    //根据毕业愿望查询学生
    List<Student> queryWish(@Param("wish") String wish);

    //根据专业查询年级
    List<String> queryGrade(@Param("studentMajor") String studentMajor);

    //根据专业，年级查询班级
    List<String> queryClass(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade);

    //根据专业查询所有学生
    List<Student> queryStudentByMajor(@Param("studentMajor") String studentMajor);

    //根据专业年级查询学生
    List<Student> queryStudentByGrade(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade);

    //根据专业年级班级查询所有学生
    List<Student> queryStudentByClass(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade,@Param("studentClass") String studentClass);

    //根据专业年级班级意愿查询学生
    List<Student> queryStudentByWish(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade,@Param("studentClass") String studentClass,@Param("wish") String wish);

    //查询特定班级学生的考研人数
    int queryStudentPost(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade,@Param("studentClass") String studentClass);
    //查询特定班级学生的就业人数
    int queryStudentEmployee(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade,@Param("studentClass") String studentClass);
    //查询特定班级学生的创业人数
    int queryStudentBusiness(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade,@Param("studentClass") String studentClass);
    //查询特定班级学生的其他人数
    int queryStudentOther(@Param("studentMajor") String studentMajor,@Param("studentGrade") String studentGrade,@Param("studentClass") String studentClass);

    //管理员修改学生
    int updateStudent(Student student);

    //添加一名学生
    int addStudent(Student student);

    //根据学生Id删除学生
    int deleteById(@Param("studentId") String studentId);
}
