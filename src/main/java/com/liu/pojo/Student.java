package com.liu.pojo;

import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String studentId;
    private String studentName;
    private String studentSex;
    private String studentPwd;
    private Integer studentAge;
    private String studentNation;
    private String studentGrade;
    private String studentClass;
    private String studentMajor;
    private Integer studentBuild;
    private String studentDormitory;
    private String studentProvince;
    private String studentCity;
    private String studentArea;
    private String headmaster;
    private String instructor;
    private String wish;
    private String study;
    private String plan;
    private String thought;
}
