package com.liu.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String admin = (String) request.getSession().getAttribute("adminId");
        String teacher = (String) request.getSession().getAttribute("teacherId");
        String student = (String) request.getSession().getAttribute("studentId");
        if(admin==null&&teacher==null&&student==null){
            request.setAttribute("msg","没有权限，请先登录");
            request.getRequestDispatcher("/index.html").forward(request,response);
            return false;
        }else{
            return true;
        }
    }
}
