package com.liu.controller;

import com.liu.mapper.AdminMapper;
import com.liu.pojo.Admin;
import com.liu.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    @Qualifier("adminServiceImpl")
    private AdminService adminService;
//    @Autowire
//    private AdminMapper adminMapper;
    //管理员登录
    @ResponseBody
    @RequestMapping("/login")
    public String adminLogin(String adminId, String adminPwd, HttpSession session){
        Admin admin = adminService.queryByIdName(adminId, adminPwd);
        String msg="";
        if(admin!=null){
            session.setAttribute("admin",admin);
            session.setAttribute("adminId",admin.getAdminId());
            msg = "1";
        }else{
            msg = "0";
        }
        return msg;
    }

    @RequestMapping("/allAdmin")
    public Admin admin(){
        Admin admin = adminService.queryAllAdmin();
        return admin;
    }

    @RequestMapping("/toAdd")
    public String toAdd(HttpSession session, Model model){
        Admin admin = (Admin) session.getAttribute("admin");
        model.addAttribute("admin",admin);
        return "admin/add";
    }

    //管理员注销
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.invalidate();
        return "index";
    }

}
