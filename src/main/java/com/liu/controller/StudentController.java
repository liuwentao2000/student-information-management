package com.liu.controller;

import com.liu.pojo.Admin;
import com.liu.pojo.Student;
import com.liu.pojo.Teacher;
import com.liu.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    @Qualifier("studentServiceImpl")
    private StudentService studentService;


    //返回到登录页面
    @RequestMapping("/toLogin")
    public String toLogin(){
        return "index";
    }


    //学生登录
    @ResponseBody
    @RequestMapping("/login")
    public String studentLogin(String studentId, String studentPwd, HttpSession session){
        Student student = studentService.queryByIdName(studentId, studentPwd);
        String msg = "";
        if(student!=null){
            session.setAttribute("student",student);
            session.setAttribute("studentId",student.getStudentId());
            msg = "1";
        }else{
            msg = "0";
        }
        return msg;
    }
    //学生注销
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.invalidate();
        return "index";
    }

    //展示学生详情
    @RequestMapping("/queryStudent")
    public String queryStudent(HttpSession session, Model model){
        Student student = (Student) session.getAttribute("student");
        Student student1 = studentService.queryStudentById(student.getStudentId());
        session.setAttribute("student",student1);
        model.addAttribute("student",student1);
        return "student/home";
    }
    //体跳转到修改页面
    @RequestMapping("/toUpdate")
    public String toUpdate(HttpSession session,Model model){
        Student student = (Student) session.getAttribute("student");
        model.addAttribute("student",student);
        return "student/update";
    }

    //修改学生信息
    @ResponseBody
    @RequestMapping("/update")
    public int update(String studentId,String studentName,int studentAge,
                         String studentBuild,String studentDormitory,String wish,String studentPwd){
        System.out.println(studentPwd);
        int i = studentService.updateById(studentId, studentName, studentAge, studentBuild, studentDormitory, wish,studentPwd);
        return i;
    }

    //教师首页
    @RequestMapping("/queryPageStudent")
    public String queryPageStudent(int page, HttpSession session, Model model){
        if(page<0){page=0;}
        List<Student> students = studentService.queryPageStudent(page*9);
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        model.addAttribute("page",page);
        model.addAttribute("students",students);
        model.addAttribute("teacher",teacher);
        return "teacher/teacherHome";
    }
    //管理员首页
    @RequestMapping("/queryStudentByAdmin")
    public String queryStudentByAdmin(int page, HttpSession session, Model model){
        if(page<0){page=0;}
        List<Student> students = studentService.queryPageStudent(page*9);
        Admin admin = (Admin) session.getAttribute("admin");
        model.addAttribute("page",page);
        model.addAttribute("students",students);
        model.addAttribute("admin",admin);
        return "admin/adminHome";
    }

    //管理员修改学生

    @ResponseBody
    @RequestMapping("/updateStudent")
    public int updateStudent(Student student){
        int i = studentService.updateStudent(student);
        return i;
    }

    //管理员添加一名学生
    @ResponseBody
    @RequestMapping("/addStudent")
    public int addStudent(Student student){
        int i = studentService.addStudent(student);
        return i;
    }

    //教师
    //根据学生Id查看学生详情
    @RequestMapping("/queryStudentById/{studentId}")
    public String queryStudentById(@PathVariable("studentId") String studentId, HttpSession session, Model model){
        Student student = studentService.queryStudentById(studentId);
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        model.addAttribute("student",student);
        model.addAttribute("teacher",teacher);
        return "teacher/teacherList";
    }

    //管理员
    //根据学生Id查看学生详情
    @RequestMapping("/queryByStudentId/{studentId}")
    public String queryByStudentId(@PathVariable("studentId") String studentId, HttpSession session, Model model){
        Student student = studentService.queryStudentById(studentId);
        Admin admin = (Admin) session.getAttribute("admin");
        model.addAttribute("student",student);
        model.addAttribute("admin",admin);
        return "admin/adminList";
    }

    //管理员根据学生Id查找学生
    @ResponseBody
    @RequestMapping("/queryById")
    public Student queryById(String studentId){
        Student student = studentService.queryStudentById(studentId);
        return student;
    }

    //全体学生图表页面
    @RequestMapping("/chart")
    public String chart(Model model,HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        int p = studentService.queryWishPost();
        int e = studentService.queryWishEmploy();
        int b = studentService.queryWishBusiness();
        int o = studentService.queryWishOther();
        System.out.println(p);
        float num = p+e+b+o;
        float post = (p/num)*100;
        float employ = (e/num)*100;
        float business = (b/num)*100;
        float other = (o/num)*100;
        System.out.println(num+","+post);
        model.addAttribute("post",post);
        model.addAttribute("employ",employ);
        model.addAttribute("business",business);
        model.addAttribute("other",other);
        model.addAttribute("p",p);
        model.addAttribute("e",e);
        model.addAttribute("b",b);
        model.addAttribute("o",o);
        model.addAttribute("teacher",teacher);
        return "teacher/teacherChart";
    }

    //跳转到查询信息页面
    @RequestMapping("/toTeacherQuery")
    public String toTeacherQuery(int page, HttpSession session, Model model){
        if(page<0){page=0;}
        List<Student> students = studentService.queryPageStudent(page*9);
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        model.addAttribute("page",page);
        model.addAttribute("students",students);
        model.addAttribute("teacher",teacher);
        return "teacher/teacherQuery";
    }

    //跳转到详情页面
    @RequestMapping("/toTeacherDetail")
    public String toTeacherDetail(int page, HttpSession session, Model model){
        if(page<0){page=0;}
        List<Student> students = studentService.queryPageStudent(page*9);
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        model.addAttribute("page",page);
        model.addAttribute("students",students);
        model.addAttribute("teacher",teacher);
        return "teacher/teacherDetail";
    }

    //根据省份查询学生
    @ResponseBody
    @RequestMapping("/queryProvince")
    public List<Student> queryProvince(String studentProvince,HttpSession session){
        List<Student> students = studentService.queryProvince(studentProvince);
        return students;
    }

    //根据辅导员查询学生
    @ResponseBody
    @RequestMapping("/queryInstructor")
    public List<Student> queryInstructor(String instructor){
        List<Student> students = studentService.queryInstructor(instructor);
        return students;
    }

    //根据毕业愿望查询学生
    @ResponseBody
    @RequestMapping("/queryWish")
    public List<Student> queryWish(String wish){
        List<Student> students = studentService.queryWish(wish);
        return students;
    }

    //根据专业查询年级
    @ResponseBody
    @RequestMapping("/queryGrade")
    public List<String> queryGrade(String studentMajor){
        List<String> s = studentService.queryGrade(studentMajor);
        return s;
    }

    //根据专业，年级查询班级
    @ResponseBody
    @RequestMapping("/queryClass")
    public List<String> queryClass(String studentMajor,String studentGrade){
        List<String> strings = studentService.queryClass(studentMajor, studentGrade);
        return strings;
    }

    //根据专业查询所有学生
    @ResponseBody
    @RequestMapping("/queryStudentByMajor")
    public List<Student> queryStudentByMajor(String studentMajor){
        List<Student> studentsByMajor = studentService.queryStudentByMajor(studentMajor);
        return studentsByMajor;
    }

    //根据专业，年级查询所有学生
    @ResponseBody
    @RequestMapping("/queryStudentByGrade")
    public List<Student> queryStudentByGrade(String studentMajor,String studentGrade){
        List<Student> students = studentService.queryStudentByGrade(studentMajor, studentGrade);
        return students;
    }

    //根据专业，年级，班级查询所有学生
    @ResponseBody
    @RequestMapping("/queryStudentByClass")
    public List<Student> queryStudentByClass(String studentMajor,String studentGrade,String studentClass){
        List<Student> students = studentService.queryStudentByClass(studentMajor, studentGrade,studentClass);
        return students;
    }

    //根据专业，年级，班级，意愿查询所有学生
    @ResponseBody
    @RequestMapping("/queryStudentByWish")
    public List<Student> queryStudentByWish(String studentMajor,String studentGrade,String studentClass,String wish){
        List<Student> students = studentService.queryStudentByWish(studentMajor, studentGrade,studentClass,wish);
        return students;
    }

    //特定班级学生图标页面
    @ResponseBody
    @RequestMapping("/queryStudentByAll")
    public List queryStudentByAll(Model model, String studentMajor, String studentGrade, String studentClass){
        List list = new ArrayList();
        int p = studentService.queryStudentPost(studentMajor,studentGrade,studentClass);
        int e = studentService.queryStudentEmployee(studentMajor,studentGrade,studentClass);
        int b = studentService.queryStudentBusiness(studentMajor,studentGrade,studentClass);
        int o = studentService.queryStudentOther(studentMajor,studentGrade,studentClass);
        System.out.println(p);
        float num = p+e+b+o;
        float post = (p/num)*100;
        float employ = (e/num)*100;
        float business = (b/num)*100;
        float other = (o/num)*100;
        list.add(post);
        list.add(employ);
        list.add(business);
        list.add(other);
        System.out.println(num+","+post);
        System.out.println(list.get(0));
        list.add(p);
        list.add(e);
        list.add(b);
        list.add(o);
        return list;
    }

    //根据学生Id删除学生
    @ResponseBody
    @RequestMapping("/deleteById")
    public int deleteById(String studentId){
        System.out.println(studentId);
        int i = studentService.deleteById(studentId);
        return i;
    }


}
