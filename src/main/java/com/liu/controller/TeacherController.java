package com.liu.controller;

import com.liu.pojo.Teacher;
import com.liu.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    @Qualifier("teacherServiceImpl")
    private TeacherService teacherService;

    @ResponseBody
    @RequestMapping("/allTeacher")
    public String queryAllTeacher(){
        Teacher teacher = teacherService.queryAllTeacher();
        return teacher.toString();
    }

    //教师登录
    @ResponseBody
    @RequestMapping("/login")
    public String teacherLogin(String teacherId, String teacherPwd, HttpSession session){
        Teacher teacher = teacherService.queryByIdName(teacherId,teacherPwd);
        String msg = "";
        if(teacher!=null){
            msg = "1";
            session.setAttribute("teacher",teacher);
            session.setAttribute("teacherId",teacher.getTeacherId());
        }else{
            msg = "0";
        }
        return msg;
    }

    //教师注销
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.invalidate();
        return "index";
    }



}
