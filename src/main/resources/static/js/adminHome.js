function page(n) {
    $(window).attr("location","/student/queryStudentByAdmin?page="+n);
}
function tobefore() {
    var pageIndex = document.getElementById("pageIndex");
    var x = parseInt(pageIndex.innerText-2)
    $(window).attr("location","/student/queryStudentByAdmin?page="+x);
}
function toafter() {
    var pageIndex = document.getElementById("pageIndex");
    var x = pageIndex.innerText;
    $(window).attr("location","/student/queryStudentByAdmin?page="+x);
}

function addStudent() {
    $.ajax({
        url:"/student/addStudent",
        data:{"studentId":$("#studentId").val(),"studentName":$("#studentName").val(),"studentSex":$("#studentSex").val(),
            "studentPwd":$("#studentPwd").val(),"studentAge":$("#studentAge").val(),"studentNation":$("#studentNation").val(),
            "studentGrade":$("#studentGrade").val(),"studentClass":$("#studentClass").val(),"studentMajor":$("#studentMajor").val(),
            "studentBuild":$("#studentBuild").val(),"studentDormitory":$("#studentDormitory").val(),"studentProvince":$("#studentProvince").val(),
            "studentCity":$("#studentCity").val(),"studentArea":$("#studentArea").val(),"headmaster":$("#headmaster").val(),
            "instructor":$("#instructor").val(),"wish":$("#wish").val(),"study":$("#study").val(),
            "plan":$("#plan").val(),"thought":$("#thought").val()},
        success:function (data) {
            if(data.toString()=="1"){
                $(window).attr("location","/student/queryStudentByAdmin?page=0");
            }
        }
    });
}

function updateStudent() {
    $.ajax({
        url:"/student/updateStudent",
        data:{"studentId":$("#studentId").val(),"studentName":$("#studentName").val(),"studentSex":$("#studentSex").val(),
            "studentPwd":$("#studentPwd").val(),"studentAge":$("#studentAge").val(),"studentNation":$("#studentNation").val(),
            "studentGrade":$("#studentGrade").val(),"studentClass":$("#studentClass").val(),"studentMajor":$("#studentMajor").val(),
            "studentBuild":$("#studentBuild").val(),"studentDormitory":$("#studentDormitory").val(),"studentProvince":$("#studentProvince").val(),
            "studentCity":$("#studentCity").val(),"studentArea":$("#studentArea").val(),"headmaster":$("#headmaster").val(),
            "instructor":$("#instructor").val(),"wish":$("#wish").val(),"study":$("#study").val(),
            "plan":$("#plan").val(),"thought":$("#thought").val()},
        success:function (data) {
            if(data.toString()=="1"){
                $(window).attr("location","/student/queryStudentByAdmin?page=0");
            }
        }
    });
}

function deleteById(studentId) {
    var r = confirm("确定删除该学生？");
    if(r == true){
        $.ajax({
            url:"/student/deleteById",
            data:{"studentId":studentId},
            success:function (data) {
                if(data.toString()=="1"){
                    $(window).attr("location","/student/queryStudentByAdmin?page=0");
                }else{
                    alert("删除失败！")
                }
            }
        });
    }

}
function query() {
    $("#nav-bottom").css("display","none")
    $.ajax({
        url:"/student/queryById",
        data:{"studentId":$("#id").val()},
        success:function (data) {
            console.log(data.toString())
            if(data.toString()){
                var html = ""
                html = "<tr>"+
                    "<td>"+data.studentId+"</td>"+
                    "<td>"+data.studentName+"</td>"+
                    "<td>"+data.studentSex+"</td>"+
                    "<td>"+data.studentMajor+"</td>"+
                    "<td>"+data.studentGrade+"</td>"+
                    "<td>"+
                        "<a class='btn btn-primary' href='/student/queryByStudentId/"+data.studentId+"'>"+"修改"+"</a>"+"|"+
                        "<a class='btn btn-danger' onclick='deleteById("+data.studentId+")'>"+"修改"+"</a>"+
                    "</td>"+
                    "</tr>";
                $("#tb").html(html)
            }else{
                alert("查询无果！")
            }
        }
    });
}