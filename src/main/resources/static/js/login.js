function studentLogin(){
    var head = document.getElementById("heading");
    head.innerHTML="学生登录";
}
function teacherLogin() {
    var head = document.getElementById("heading");
    head.innerHTML="教师登录";
}
function adminLogin() {
    var head = document.getElementById("heading");
    head.innerHTML="系统登录";
}
function login() {
    var heading = document.getElementById("heading")
    if (heading.innerText=="学生登录"){
        $.ajax({
            url:"/student/login",
            data:{"studentId":$("#userId").val(),"studentPwd":$("#userPwd").val()},
            success:function (data) {
                if(data.toString()=="1"){
                    $(window).attr("location","/student/queryStudent");
                }else{
                    alert("账号或密码错误！");
                }
            }
        });
    }else if(heading.innerText=="教师登录"){
        $.ajax({
            async:false,
            url:"/teacher/login",
            data:{"teacherId":$("#userId").val(),"teacherPwd":$("#userPwd").val()},
            success:function (data) {
                if(data.toString()=="1"){
                    $(window).attr("location","/student/queryPageStudent?page=0");
                }else{
                    alert("账号或密码错误！");
                }
            }
        });
    }else{
        $.ajax({
            url:"/admin/login",
            data:{"adminId":$("#userId").val(),"adminPwd":$("#userPwd").val()},
            success:function (data) {
                if(data.toString()=="1"){
                    $(window).attr("location","/student/queryStudentByAdmin?page=0");
                }else{
                    alert("账号或密码错误！");
                }
            }
        });
    }
}
