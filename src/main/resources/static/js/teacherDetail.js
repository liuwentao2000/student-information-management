function page(n) {
    $(window).attr("location","/student/toTeacherDetail?page="+n);
}
function tobefore() {
    var pageIndex = document.getElementById("pageIndex");
    var x = parseInt(pageIndex.innerText-2)
    $(window).attr("location","/student/toTeacherDetail?page="+x);
}
function toafter() {
    var pageIndex = document.getElementById("pageIndex");
    var x = pageIndex.innerText;
    $(window).attr("location","/student/toTeacherDetail?page="+x);
}
function queryGrade() {
    $.ajax({
        url:"/student/queryGrade",
        data:{"studentMajor":$("#major").val()},
        success:function (data) {
            if(data.toString()!=null){
                var html = ""
                for (let i=0;i<data.length;i++){
                    html += "<option>"+data[i]+"</option>"
                }
                $("#grade").html(html)
            }
        }
    });
}
function queryClass() {
    $.ajax({
        url:"/student/queryClass",
        data:{"studentMajor":$("#major").val(),"studentGrade":$("#grade").val()},
        success:function (data) {
            if(data.toString()!=null){
                var html = ""
                for (let i=0;i<data.length;i++){
                    html += "<option>"+data[i]+"</option>"
                }
                $("#class").html(html)
            }
        }
    });
}
function queryStudentByMajor(n) {
    $.ajax({
        url:"/student/queryStudentByMajor",
        data:{"studentMajor":$("#major").val()},
        success:function (data) {
            function showList(min,max) {
                var html = ""
                for (let i=min;i<max;i++){
                    html += "<tr>"+
                        "<td>"+data[i].studentId+"</td>"+
                        "<td>"+data[i].studentName+"</td>"+
                        "<td>"+data[i].studentSex+"</td>"+
                        "<td>"+data[i].studentMajor+"</td>"+
                        "<td>"+data[i].studentGrade+"</td>"+
                        "<td>"+"<a class='btn btn-primary' href='/student/queryStudentById/"+data[i].studentId+"'>"+"详情</a>"+
                        "</tr>"
                }
                $("#tb").html(html)
            }
            if(data.toString()!=null){
                let num = data.length;
                let pageIndex = Math.ceil(num/9);
                if(n==1&&pageIndex!=1){
                    showList(0,9)
                }else if (n==1&&pageIndex==1){
                    showList(0,num)
                }else if(n==pageIndex){
                    showList((n-1)*9,num)
                }else{
                    showList((n-1)*9,n*9)
                }
                var li = ""
                li = "<ul class='pagination'>"+
                    "<li>"+"<a onclick='queryStudentByMajor("+(n-1)+")'>"+"&laquo;"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByMajor(1)'>"+"1"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByMajor(2)'>"+"2"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByMajor(3)'>"+"3"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByMajor(4)'>"+"4"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByMajor(5)'>"+"5"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByMajor("+(n+1)+")'>"+"&raquo;"+"</a>"+"</li>"+
                    "</ul>"
                $("#navli").html(li)
            }else {
                alert("不存在！");
            }
        }
    });
}
function queryStudentByGrade(n) {
    $.ajax({
        url:"/student/queryStudentByGrade",
        data:{"studentMajor":$("#major").val(),"studentGrade":$("#grade").val()},
        success:function (data) {
            function showList(min,max) {
                var html = ""
                for (let i=min;i<max;i++){
                    html += "<tr>"+
                        "<td>"+data[i].studentId+"</td>"+
                        "<td>"+data[i].studentName+"</td>"+
                        "<td>"+data[i].studentSex+"</td>"+
                        "<td>"+data[i].studentMajor+"</td>"+
                        "<td>"+data[i].studentGrade+"</td>"+
                        "<td>"+"<a class='btn btn-primary' href='/student/queryStudentById/"+data[i].studentId+"'>"+"详情</a>"+
                        "</tr>"
                }
                $("#tb").html(html)
            }
            if(data.toString()!=null){
                let num = data.length;
                let pageIndex = Math.ceil(num/9);
                if(n==1&&pageIndex!=1){
                    showList(0,9)
                }else if (n==1&&pageIndex==1){
                    showList(0,num)
                }else if(n==pageIndex){
                    showList((n-1)*9,num)
                }else{
                    showList((n-1)*9,n*9)
                }
                var li = ""
                li = "<ul class='pagination'>"+
                    "<li>"+"<a onclick='queryStudentByGrade("+(n-1)+")'>"+"&laquo;"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByGrade(1)'>"+"1"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByGrade(2)'>"+"2"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByGrade(3)'>"+"3"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByGrade(4)'>"+"4"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByGrade(5)'>"+"5"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByGrade("+(n+1)+")'>"+"&raquo;"+"</a>"+"</li>"+
                    "</ul>"
                $("#navli").html(li)
            }else {
                alert("不存在！");
            }
        }
    });
}
function queryStudentByClass(n) {
    $.ajax({
        url:"/student/queryStudentByClass",
        data:{"studentMajor":$("#major").val(),"studentGrade":$("#grade").val(),"studentClass":$("#class").val()},
        success:function (data) {
            function showList(min,max) {
                var html = ""
                for (let i=min;i<max;i++){
                    html += "<tr>"+
                        "<td>"+data[i].studentId+"</td>"+
                        "<td>"+data[i].studentName+"</td>"+
                        "<td>"+data[i].studentSex+"</td>"+
                        "<td>"+data[i].studentMajor+"</td>"+
                        "<td>"+data[i].studentGrade+"</td>"+
                        "<td>"+"<a class='btn btn-primary' href='/student/queryStudentById/"+data[i].studentId+"'>"+"详情</a>"+
                        "</tr>"
                }
                $("#tb").html(html)
            }
            if(data.toString()!=null){
                let num = data.length;
                let pageIndex = Math.ceil(num/9);
                if(n==1&&pageIndex!=1){
                    showList(0,9)
                }else if (n==1&&pageIndex==1){
                    showList(0,num)
                }else if(n==pageIndex){
                    showList((n-1)*9,num)
                }else{
                    showList((n-1)*9,n*9)
                }
                var li = ""
                li = "<ul class='pagination'>"+
                    "<li>"+"<a onclick='queryStudentByClass("+(n-1)+")'>"+"&laquo;"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByClass(1)'>"+"1"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByClass(2)'>"+"2"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByClass(3)'>"+"3"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByClass(4)'>"+"4"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByClass(5)'>"+"5"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByClass("+(n+1)+")'>"+"&raquo;"+"</a>"+"</li>"+
                    "</ul>"
                $("#navli").html(li)
            }else {
                alert("不存在！");
            }
        }
    });
}
function queryStudentByWish(n) {
    $.ajax({
        url:"/student/queryStudentByWish",
        data:{"studentMajor":$("#major").val(),"studentGrade":$("#grade").val(),"studentClass":$("#class").val(),"wish":$("#wish").val()},
        success:function (data) {
            function showList(min,max) {
                var html = ""
                for (let i=min;i<max;i++){
                    html += "<tr>"+
                        "<td>"+data[i].studentId+"</td>"+
                        "<td>"+data[i].studentName+"</td>"+
                        "<td>"+data[i].studentSex+"</td>"+
                        "<td>"+data[i].studentMajor+"</td>"+
                        "<td>"+data[i].studentGrade+"</td>"+
                        "<td>"+"<a class='btn btn-primary' href='/student/queryStudentById/"+data[i].studentId+"'>"+"详情</a>"+
                        "</tr>"
                }
                $("#tb").html(html)
            }
            if(data.toString()!=null){
                let num = data.length;
                let pageIndex = Math.ceil(num/9);
                if(n==1&&pageIndex!=1){
                    showList(0,9)
                }else if (n==1&&pageIndex==1){
                    showList(0,num)
                }else if(n==pageIndex){
                    showList((n-1)*9,num)
                }else{
                    showList((n-1)*9,n*9)
                }
                var li = ""
                li = "<ul class='pagination'>"+
                    "<li>"+"<a onclick='queryStudentByWish("+(n-1)+")'>"+"&laquo;"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByWish(1)'>"+"1"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByWish(2)'>"+"2"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByWish(3)'>"+"3"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByWish(4)'>"+"4"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByWish(5)'>"+"5"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryStudentByWish("+(n+1)+")'>"+"&raquo;"+"</a>"+"</li>"+
                    "</ul>"
                $("#navli").html(li)
            }else {
                alert("不存在！");
            }
        }
    });
}
function  queryStudentByAllPie() {
    $("#btnChart").css("display","block")
    $.ajax({
        url:"/student/queryStudentByAll",
        data:{"studentMajor":$("#major").val(),"studentGrade":$("#grade").val(),"studentClass":$("#class").val()},
        success:function (data) {
            if(data.toString()!=null){
                $(document).ready(function() {
                    var post = data[0]
                    var employ = data[1]
                    var business = data[2]
                    var other = data[3]
                    console.log(post)
                    var chart = {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    };
                    var title = {
                        text: $("#major").val()+$("#grade").val()+'级'+$("#class").val()+'班'+'学生毕业愿望'
                    };
                    var tooltip = {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    };
                    var plotOptions = {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    };
                    var series= [{
                        type: 'pie',
                        name: '毕业愿望占比',
                        data: [
                            ['考研',   post],
                            ['就业',   employ],
                            {
                                name: '创业',
                                y: business,
                                sliced: true,
                                selected: true
                            },
                            ['其他',   other]
                        ]
                    }];
                    var json = {};
                    json.chart = chart;
                    json.title = title;
                    json.tooltip = tooltip;
                    json.series = series;
                    json.plotOptions = plotOptions;
                    $('#pieChart').highcharts(json);
                });
                $("#table").css("display","none")
                $("#navli").css("display","none")
                $("#columnChart").css("display","none")
                $("#pieChart").css("display","block")
            }
        }
    });
}
function queryStudentByAllColumn() {
    $("#btnChart").css("display","block")
    $.ajax({
        url:"/student/queryStudentByAll",
        data:{"studentMajor":$("#major").val(),"studentGrade":$("#grade").val(),"studentClass":$("#class").val()},
        success:function (data) {
            if(data.toString()!=null){
                $(document).ready(function() {
                    var p = data[4]
                    var e = data[5]
                    var b = data[6]
                    var o = data[7]
                    var chart = {
                        type: 'column'
                    };
                    var title = {
                        text: $("#major").val()+$("#grade").val()+'级'+$("#class").val()+'班'+'学生毕业意愿'
                    };
                    var subtitle = {
                        text: '柱形图'
                    };
                    var xAxis = {
                        categories: ['考研','就业','创业','其他'],
                        crosshair: true
                    };
                    var yAxis = {
                        min: 0,
                        title: {
                            text: '人数'
                        }
                    };
                    var tooltip = {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    };
                    var plotOptions = {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    };
                    var credits = {
                        enabled: false
                    };

                    var series= [{
                        name: '学生人数',
                        data: [p, e, b, o]
                    }];

                    var json = {};
                    json.chart = chart;
                    json.title = title;
                    json.subtitle = subtitle;
                    json.tooltip = tooltip;
                    json.xAxis = xAxis;
                    json.yAxis = yAxis;
                    json.series = series;
                    json.plotOptions = plotOptions;
                    json.credits = credits;
                    $('#columnChart').highcharts(json);
                });
                $("#table").css("display","none")
                $("#navli").css("display","none")
                $("#pieChart").css("display","none")
                $("#columnChart").css("display","block")
            }
        }
    });
}