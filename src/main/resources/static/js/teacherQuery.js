function queryProvince(n) {
    $.ajax({
        url:"/student/queryProvince",
        data:{"studentProvince":$("#province").val()},
        success:function (data) {
            if(data.toString()){
                function showList(min,max) {
                    var html = ""
                    for (let i=min;i<max;i++){
                        html += "<tr>"+
                            "<td>"+data[i].studentId+"</td>"+
                            "<td>"+data[i].studentName+"</td>"+
                            "<td>"+data[i].studentSex+"</td>"+
                            "<td>"+data[i].studentMajor+"</td>"+
                            "<td>"+data[i].studentGrade+"</td>"+
                            "<td>"+"<a class='btn btn-primary' href='/student/queryStudentById/"+data[i].studentId+"'>"+"详情</a>"+
                            "</tr>"
                    }
                    $("#tb").html(html)
                }
                let num = data.length;
                let pageIndex = Math.ceil(num/9);
                if(n==1&&pageIndex!=1){
                    showList(0,9)
                }else if (n==1&&pageIndex==1){
                    showList(0,num)
                }else if(n==pageIndex){
                    showList((n-1)*9,num)
                }else{
                    showList((n-1)*9,n*9)
                }
                var li = ""
                li = "<ul class='pagination'>"+
                    "<li>"+"<a onclick='queryProvince("+(n-1)+")'>"+"&laquo;"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryProvince(1)'>"+"1"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryProvince(2)'>"+"2"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryProvince(3)'>"+"3"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryProvince(4)'>"+"4"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryProvince(5)'>"+"5"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryProvince("+(n+1)+")'>"+"&raquo;"+"</a>"+"</li>"+
                    "</ul>"
                $("#navli").html(li)
            }else {
                alert("查询无果！");
            }
        }
    });
}

function queryInstructor(n) {
    $.ajax({
        url:"/student/queryInstructor",
        data:{"instructor":$("#instructor").val()},
        success:function (data) {
            if(data.toString()){
                function showList(min,max) {
                    var html = ""
                    for (let i=min;i<max;i++){
                        html += "<tr>"+
                            "<td>"+data[i].studentId+"</td>"+
                            "<td>"+data[i].studentName+"</td>"+
                            "<td>"+data[i].studentSex+"</td>"+
                            "<td>"+data[i].studentMajor+"</td>"+
                            "<td>"+data[i].studentGrade+"</td>"+
                            "<td>"+"<a class='btn btn-primary' href='/student/queryStudentById/"+data[i].studentId+"'>"+"详情</a>"+
                            "</tr>"
                    }
                    $("#tb").html(html)
                }
                let num = data.length;
                let pageIndex = Math.ceil(num/9);
                if(n==1&&pageIndex!=1){
                    showList(0,9)
                }else if (n==1&&pageIndex==1){
                    showList(0,num)
                }else if(n==pageIndex){
                    showList((n-1)*9,num)
                }else{
                    showList((n-1)*9,n*9)
                }
                var li = ""
                li = "<ul class='pagination'>"+
                    "<li>"+"<a onclick='queryInstructor("+(n-1)+")'>"+"&laquo;"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryInstructor(1)'>"+"1"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryInstructor(2)'>"+"2"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryInstructor(3)'>"+"3"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryInstructor(4)'>"+"4"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryInstructor(5)'>"+"5"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryInstructor("+(n+1)+")'>"+"&raquo;"+"</a>"+"</li>"+
                    "</ul>"
                $("#navli").html(li)
            }else{
                alert("查询无果！");
            }
        }
    });
}

function queryWish(n) {
    $.ajax({
        url:"/student/queryWish",
        data:{"wish":$("#wish").val()},
        success:function (data) {
            if(data.toString()){
                function showList(min,max) {
                    var html = ""
                    for (let i=min;i<max;i++){
                        html += "<tr>"+
                            "<td>"+data[i].studentId+"</td>"+
                            "<td>"+data[i].studentName+"</td>"+
                            "<td>"+data[i].studentSex+"</td>"+
                            "<td>"+data[i].studentMajor+"</td>"+
                            "<td>"+data[i].studentGrade+"</td>"+
                            "<td>"+"<a class='btn btn-primary' href='/student/queryStudentById/"+data[i].studentId+"'>"+"详情</a>"+
                            "</tr>"
                    }
                    $("#tb").html(html)
                }
                let num = data.length;
                let pageIndex = Math.ceil(num/9);
                if(n==1&&pageIndex!=1){
                    showList(0,9)
                }else if (n==1&&pageIndex==1){
                    showList(0,num)
                }else if(n==pageIndex){
                    showList((n-1)*9,num)
                }else{
                    showList((n-1)*9,n*9)
                }
                var li = ""
                li = "<ul class='pagination'>"+
                    "<li>"+"<a onclick='queryWish("+(n-1)+")'>"+"&laquo;"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryWish(1)'>"+"1"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryWish(2)'>"+"2"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryWish(3)'>"+"3"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryWish(4)'>"+"4"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryWish(5)'>"+"5"+"</a>"+"</li>"+
                    "<li>"+"<a onclick='queryWish("+(n+1)+")'>"+"&raquo;"+"</a>"+"</li>"+
                    "</ul>"
                $("#navli").html(li)
            }
            else{
                alert("查询无果！");
            }
        }
    });
}


function page(n) {
    $(window).attr("location","/student/toTeacherQuery?page="+n);
}
function tobefore() {
    var pageIndex = document.getElementById("pageIndex");
    var x = parseInt(pageIndex.innerText-2)
    $(window).attr("location","/student/toTeacherQuery?page="+x);
}
function toafter() {
    var pageIndex = document.getElementById("pageIndex");
    var x = pageIndex.innerText;
    $(window).attr("location","/student/toTeacherQuery?page="+x);
}
