
function page(n) {
    $(window).attr("location","/student/queryPageStudent?page="+n);
}
function tobefore() {
    var pageIndex = document.getElementById("pageIndex");
    var x = parseInt(pageIndex.innerText-2)
    $(window).attr("location","/student/queryPageStudent?page="+x);
}
function toafter() {
    var pageIndex = document.getElementById("pageIndex");
    var x = pageIndex.innerText;
    $(window).attr("location","/student/queryPageStudent?page="+x);
}

function queryById() {
    $("#nav-bottom2").css("display","none")
    $.ajax({
        url:"/student/queryById",
        data:{"studentId":$("#stuId").val()},
        success:function (data) {
            if(data.toString()){
                var html = ""
                html = "<tr>"+
                    "<td>"+data.studentId+"</td>"+
                    "<td>"+data.studentName+"</td>"+
                    "<td>"+data.studentSex+"</td>"+
                    "<td>"+data.studentMajor+"</td>"+
                    "<td>"+data.studentGrade+"</td>"+
                    "<td>"+
                    "<a class='btn btn-primary' href='/student/queryStudentById/"+data.studentId+"'>"+"详情"+"</a>"+
                    "</td>"+
                    "</tr>";
                $("#tb2").html(html)
            }else{
                alert("查询无果！")
            }
        }
    });
}